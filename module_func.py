def fact(x):
    otvet = 1
    for i in range(1, x +1):
        otvet = otvet * i
    return otvet

def mul(x):
    for i in range(x):
        print(str(i) + '! =\t' + str(fact(i)))

def create_record(name, phone, address):
    record = {
        'name': name,
        'phone': phone,
        'address': address
    }
    return record