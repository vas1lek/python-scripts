


enemy = {
            'loc_x': 70,
            'loc_y': 50,
            'color': 'green',
            'health': 100,
            'name': 'Mudilla'
}

print(enemy)

print("Location X = " + str(enemy['loc_x']))
print("Location Y = " + str(enemy['loc_y']))
print("His name is = " + str(enemy['name']))

enemy['rank'] = 'Admiral'  # добавление item в enemy (в словарь)
print(enemy)

del enemy['rank']   # удаление item
print(enemy)

enemy['loc_x'] = enemy['loc_x'] + 40    # меняем значение
enemy['health'] = enemy['health'] - 30   # меняем значение

if enemy['health'] < 80:
    enemy['color'] = 'yellow'

print(enemy)

print(enemy.keys())   # ключи
print(enemy.values())   # значения

