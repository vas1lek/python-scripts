        # Открытие файла

student_file = open('students.txt', 'r', encoding='utf-8')

sales_file = open('sales.txt', 'w')

#file1 = open(r'C:/Users/temp/test.txt')

print(student_file.encoding)

print(student_file.closed)  # проверяем закрыт ли файл
#student_file.close() # закрываем файл
print(student_file.closed) # проверяем закрыт ли файл

        # Чтение содержимого файла 

# read() – читает все содержимое файла;
# readline() – читает одну строку из файла;

content = student_file.read(1)
print(content)