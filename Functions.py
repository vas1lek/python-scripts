import module_func
from module_func import *     # чтобы каждый раз не вызывать модуль

# 5! = 1 * 2 * 3 * 4 * 5
module_func.mul(10)

########################################

root = module_func.create_record('Daniil', '89969404732', 'Dolnyk_71')

print(root)