from class_2 import *

myHero1 = Hero("Daniil", 10, "humen")  # создаем объект Hero с параметрами
myHero2 = Hero("Myak", 8, "Goblin")

myHero2.show_hero()
myHero1.level_up()
myHero1.level = 1   # можем поменять параметр, так лучше не делать, для этого нужно создать новый метод
myHero1.show_hero()
myHero1.new_health(100)
myHero1.show_hero()

# Супер класс, наследование

superHero1 = SuperHero("Obuzok", 41, 'ORK', 9809)

superHero1.show_hero()