
# КЛАСС это шаблон по которому мы создаем ОБЪЕКТ

# ПРИНЦИПЫ ООП
# Инкапсуляция
# Абстракция
# Наследование (Доп. методы и свойства)
# Полиморфизм
#
#
class Hero():   # можно вывести в отдельный файл и написать import
    """Class to Create Hero for our Game"""
    def __init__(self, name, level, race):   # параметры объекта (СВОЙСТВА)
        self.name = name
        self.level = level
        self.race = race
        self.health = 180   # общий параметр

    def show_hero(self):   # метод 1
        descriptions = (self.name + " Level is: " + str(self.level) + " Race is: "
                        + self.race + " Health is " + str(self.health)).title()
        print(descriptions)

    def level_up(self):   # метод 2
        self.level += 1

    def move(self):   # метод 3
        print("Hero " + self.name + " start moving...")

    def new_health(self, new):
        self.health = new


class SuperHero(Hero):   # наследование
    """Class to Create Super Hero"""
    def __init__(self, name, level, race, magiclevel):
        super().__init__(name,level, race)   # перекинуть из Родительского класса
        self.magiclevel = magiclevel
        self.magic = 100

    def makemagic(self):
        """Use magic"""
        self.magic -= 10

    def show_hero(self):
        descriptions = (self.name + " Level is: " + str(self.level) + " Race is: "
                        + self.race + " Health is " + str(self.health) +
                        " Magic is: " + str(self.magic) +
                        " Magic level: " + str(self.magiclevel)).title()
        print(descriptions)